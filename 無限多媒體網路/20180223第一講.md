# 無限多媒體網路 - 第一講 Quality of Service (Qos)
> 定義 service 品質，以及如何做

## 服務品質（Quality of Service）　　　

### 一、 Introduction 
1. `目的`：提供網路效能的合約及保證，以及對使用者有 `service differention`。
例如： 中華電信提供一個月多少流量或是網速多少的方案，對於不同的使用者有不同的assurance，提供不同的service

2. `定義` ： 提供 `resource assurance`  和 `service differention` 的能力

3. `如何提供 Qos` :   
    * 資源分配機制（例如使用者跟資源的排程）
    * 定義服務品質的類別（Qos class）/指數(index)去量化服務（engineer level）,例如你說 Qos 為 4,就知道是不要掉太多封包的服務


### 二、 Qos class (Service Catagories) ppt 3
**請參考講義圖片**

這裡注意 Qos = 5 的地方，為 tradition IP ，全部都是
Undefined ，意思是盡量幫你送 package,但 loss 或是 error 都不管

< 名詞解釋 >
* `IPER` : IP packet 錯誤（如: checksum） 
* `IPTD` : IP packet transfer delay （下面補充）
* `IPLR` : IP packet loss ratio (每傳送幾個，掉封包的比例)
* `IPER` : IP packet error ratio（可能受到 noise 或是 干擾，即 error correction 的能力）

#### transfer delay (傳送延遲)

**transfer delay = propagation delay + transmission delay + queuing delay**


* `propagation delay` ： 第一個 bit 離開 Host 到另一端 Host 時間，**和傳遞距離有關**，傳輸速度為 $2 * 10^8 m/sec$，雖然幾乎可以忽視，但如果中間的纜線很長，這個就不可忽視。

* `transmission delay` : package size / link capacity, 和傳輸線材質、link capacity、封包大小有關
所以封包越大、link速度越慢、延遲越大。


* `queuing delay` : 因為網路不可能只有點對點（Host to Host），中間也有經過 router、switch、gateway等，然而他們也不是只有服務你而已。像 router 可能也會連結 subnetwork,所以也有 buffer ，故還要加上 queuing time,
和 CPU 等硬體設備有關，快的話可以看一下就送出封包。
就是在queue裡面等著被服務 ＝ 待在buffer。


像 video stream, 不可能馬上送到馬上播，像 youtube 的播放有個灰色的緩衝條，等buffer滿了再送出播一段，克服 jitter 很大的問題。

`jitter` ： 在IP網路中，每個封包到目的地的時間（延遲）長短不一， 如果將封包先儲存在 `一個dejitter buffer` 中，排序，再依既定步調逐一解碼送出， 如此可以很容易的降低 jitter的問題，所增加的是一些延遲時間。


如同 `ppt-p4` 如果 variance 上升，每個封包的延遲差異越來越嚴重，buffer loading 很重，有時buffer滿，反之有時很空。但如果 variance = 0, 每個封包延遲時間相同，就沒有jitter的問題了。

## Resource Allocation
1. `throughput 吞吐量` ： 每秒傳送真正的payload size(要正確的資料，錯誤的不計入)，要從接收者端來看

* `payload size` : 一個control message ，真正傳的資料大小，不包含表頭（header）資訊, CRC 等。

因此，throughput 一定小於 link capacity, 因為不可能只傳 payload，所以 link 即使 1G/s, 但 throughput 不會 1G/s。

##### 所以data rate 、 throughput、 link capacity不是一樣的東西喔。

2. Resource

* 對有線網路（Wired）來說，Resource 有 
    * Bandwidth(頻寬)，可能是乙太或光纖
    * buffers (router and switch)

* 對無線網路（Wireless）來說，Resource 有
    * frequency
    * time
    * phase(相位)
    * antema(space)

`Buffers overloading` : buffer 爆掉，sender 延遲時間上升或是pakcet 丟掉。看 router 是否能 maintain 這資料量（xxx MB/s）,就要開始丟封包。那丟封包有法則, 可能是 Round robin 丟, 滿了之後之後都丟之類的，決定你網路的架構及你的設備決定如何丟封包。

所以Qos（服務品質） 就會和2個機制有關:
1.你配到多少頻寬，和你的延遲（delay）時間表現就有關了
2.你拿到多少 buffer, Router 會 allocate 多少 buffer 給你這個flow，當然會跟你看到的延遲（delay）也有關,
另外一個會影響的是 packet loss probability, buffer 越大，loss probability 越大

因此服務品質牽涉到兩個機制：
1. 怎排封包出去順序
2. 把記憶體配置給不同flow, 讓他們被服務之前可以待在 queue等不會被丟掉

所以
1. 頻寬給的少，延遲時間會拉長
2. buffer 大小也會影響延遲時間，buffer 越大,queue 裡面越來越多，後面的人就會等更久


`IP network` : best effort, 盡量幫你送到，封包丟了就是丟了，也不會通知送丟，但今天的網路都會通知丟失封包是因為加了TCP, IP 在 TCP 下面，那是因為今天你想要提供比較有服務品質的，只少要說有沒有丟掉，所以才自己加了TCP。因此，在網路層下面的都是best effort。你要reliable 就要自己往上面去加，所以Qos也是加在上面的，下面才不管這件事。


### 那我們能否改變 buffer management?讓router 聰明一點,讓 buffer 滿了丟封包會通知，變聰明一點


router 在第三層，TCP 在 第四層，TCP 是 Host 對 Host(n to M，就是使用者端對伺服器端),
，**中間這些router還是不歸TCP管**,中間router封包丟掉不會通知，丟了就丟了，也不會同之前一站，而電腦收到的 ACK也是M（server）告訴你。所以TCP參與的人只有使用者端和伺服器端，中間 router 不管TCP的。

early detection : 如果存到buffer的3/4,就有機率開始丟, IP header 就會加上一個小標籤，會回去跟前一站router....一直往前回饋，告訴 sender 說需要送慢一點。但是這是是進階功能，還是要看 router version。

## Shannon's Theorem :
 $$C = B*log(1+SNR) = B*log(1+\frac{S}{N})$$

 Ex: 頻寬 20 MHz = 20Mbits/s, 但通道上不是只有 wifi而已，還有其他像 LTE 等，所以 20 MHz 能否真的送 20Mbits/s，決定於你的 SNR(訊雜比)。**SNR : 通道的 quality**。例如到菜市場，用同樣的音量講話，一大清早沒有雜訊所以很清楚，但 10 點後人很多，同樣的音量對背景雜訊而言，開始聽不清楚，可能需要重傳,或是講慢一點。

 但是對於無線通訊而言，沒有所謂的慢一點，例如一個正弦不能代表0.5個位元，因為 bit is binary, 所以在吵雜的環境下，有可能一個位元送錯，可能原來是要送高電壓(1)變成送低電壓(0)，所以才需要 reduntant bit (CRC) 去做bit, 而且 CRC 不算在 throughtput。

另外，SNR 和 Rate(傳送速度) 呈現階梯函數(step)，在某個區間裡面只有固定的某個速度。SNR 在某個範圍速度就可以調多少，這個機制在無線網路稱為 `MCS` (Modulation and Coding Scheme) coding 就是 redundant bit

[參考網頁](http://www.cash.idv.tw/wordpress/?p=5914)
[參考網頁2](http://www.purewifi.tw/?p=255)

$$codingrate = \frac{x}{y} $$

就是每送 y 個位元，有 y-x 個是 redundant bit, 而`分數越大表訊號越好`，可以用比較爛的除錯能力。Coding Scheme 就是把 redundant coding 進去訊息裡面。

而調變 Modulation，如菜市場，一大清早可以用一模一樣的音量來講話可以講快一點，所以一個賀茲可以傳 8 bits，但是 SNR 若是不好，可能只能傳 2 個bits。所以每個赫茲可以傳多少位元稱為調變。SNR越好，一個赫茲可以送的位元越多。


link capacity 和 MCS 之間的關聯:
有不同的SNR，所代進去的不會找到最理想的capacity，那就要查表了，所以網卡上面會寫多少範圍 SNR 用多少傳送速度(查表)。再去通知第一層該對應速度。
把 MAC 收到的封包加上reduntant，channel coding。

coding rate 和 CRC 不同之處

    CRC 是用網卡算，和通道無關(跟據採用的通訊標準有關)
    
    coding rate 是和通道有關 (channel coding)，和通道狀況、SNR 有關，凡是跟通道有關都是表示再實體層
    
    checksum 是 TCP/IP算


所以無線通訊無所不用其極的在做 reduntant(連實體層也做)。有線通訊只有CRC和checksum，在實體層就沒有做這事情。


假設今天通道是 20MHz ， 最快100Mbps傳輸速率，意思是 每個 Hz 要傳多少 bits 才有可能傳到這種效率。 => 5個位元 ( 5 = logX, X = 32 = 2^5)

## Resource Allocation v.s. Congestion Control

1. IP network 沒有 Resource Allocation，因為他是 best effort，沒有哪一個 router 會很好心的留資源給其他人。
2. TCP 有雍塞，甚麼時候網路使者會發現有雍塞發生? congestion indicator 是掉封包，timeout 會重傳(但只是反映error，而不是 congestion)表示 congestion，所以 congestion 對 TCP 的反應是 slow start(其實也沒有很慢)，會去探測網路的可行性，送 1 2 4 8 16... 封包，如果 16 開始沒有所有ACK都回來，臨界值設在16/2 = 8，再從一開始1 2 4 8，8個ACK回來後。之後9, 10, ...

所以沒有 Resource Allocation，每個人都盡量送封包(best effort)，到不了就會掉封包，機制就會考慮開始降下速度。所以網路效能就會不斷消長震盪，表是網路效能沒有被充分。利用但這個不是我們要的，我們要的是穩定，所以先探測網路的能力再穩穩的送。

所以要避免 Congestion Control，就要做 Resource Allocation

* 對網路節點而言
1.  Router 不要 FIFO，因為大家都會拼命想要把 queue 佔住
2.  給不同優先權(Router 要支援這種機制)，但是高優先的就一直送
3.  queueing discipline 決定先把誰從 queue 送出 (FIFO, priority)
4.  進來速度 > 出去速度，才需要做排程

* 對 Host 而言

Host 要知道可以送多快，要有 feedback 機制才知道。congestion control 就是 TCP，掉封包就是一個警訊

那 resource allocation 怎麼做? 分為 

* Router centric
全由路由器決定誰用多少資源，為現在現行的方法

* host centric: 給電腦觀察網路狀況，決定送多快。但是並不是網路的現況，是看 TCP 的 window : receriver buffer size(不是 router buffer size, TCP 管不到 router)，window 是動態的，隨者電腦使用狀況做伸縮，預設是 16 bits = 64 KB。太小了送 media 不夠用。現在硬體設備都很好，所以後來變成兩邊講好(hand shaking 可以兩面談好)


* Reservation based
送資料前要不要先預留頻寬，告訴延途的 router 你要送多少容量

* feedback based 
送資料不先跟網路節點說你要送多少，送就對了，看妳收到的 feedback，調整傳輸速率快慢。(TCP 是 implicit ，透過 ACK lost 決定送太快送太慢, explicit 是由 router 看誰送最多，告訴送最多的人送慢一點)，網路層 ICMP 協訂，如果發現誰送太多，可以送 source quench(閉嘴) 的 control message，叫他閉嘴，他就不會送了。網路不塞的時候再拿掉這個機制。

* window based
  ex : TCP 利用 window size 去做 rate control (記憶體給你這麼多，你可以塞)，可以換算多少rate是不確定的 
  
* rate based
  Ex : 每秒鐘可以送多少 bits (告訴你每秒鐘送多少bits)，如網路多媒體告
  訴你要用多少畫質


## Evalution Criteria

Resource allocation

1. 期望資源在使用上是有效率的，不要有 idle 的
2. 資源要公平分配(公平要定義)

`有效率` : 從 throughput 和 delay 的表現來看，但是這兩個是有衝突的，頻寬是 100Mbps，會想辦法把 100Mb 的資料量灌滿，但其實沒辦法真的 100Mbps，但為什麼不灌剛好要灌超過，因為網路是動態的，你就算掉封包還是可以在 100Mbps，但是灌多的結果就是都在 queue 裡面，可以有最好的  throughput，但是 delay 會上昇。 queuing delay 會上昇。

$$Power = \frac{Tthroughput}{delay}$$

目標要把 Power 最大化，很像分時系統的 throughput 
`ALOHAnet`

`公平` : 每個 flow 有相同的 bandwidth
work conservation : 不會浪費時間在 idle queue 上面，不會等 empty 有東西才去服務。

公平性 evaluation criteria `(jain's faiirness index)`

每個人 throughput = 1, 為 100 %公平
如果有一個人要用 1 + $\delta$ 剩下的人都用 1，那就不是 100%公平
## Queueing discipline
[參考網站](http://www.study-area.org/tips/2.4routing/2.4routing-big5-9.html)
1. 決定在 router 內，有哪些封包可以決定被傳送
2. Queueing algorithm 考慮分配頻寬(誰將被傳出去)及 buffer space(誰要被丟掉)

### FIFO
兩套法則，封包出去的 policy , 還要封包來時 queue 滿的 drop policy
### priority queueing
